/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.gateway.props;

import com.alibaba.nacos.common.utils.StringUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.launch.constant.TokenConstant;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * Token校验
 *
 * @author zhaoPan
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties("blade.token")
@Slf4j
public class TokenProperties {
	/**
	 * 加密串
	 */
	private String signKey;

	public String getSignKey() {
		if (StringUtils.isBlank(signKey)) {
			log.warn("Token已启用默认签名,请前往blade.token.sign-key设置32位的key");
			return TokenConstant.SIGN_KEY;
		} else {
			return signKey;
		}
	}
}
